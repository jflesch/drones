#!/usr/bin/env python3

import asyncio
# import os
import threading

import cv2

import tello_asyncio


print('[main thread] START')

##############################################################################
# drone control in worker thread


def fly():
    print('[fly thread] START')

    async def main():
        drone = tello_asyncio.Tello()
        try:
            await asyncio.sleep(1)
            # await drone.wifi_wait_for_network(prompt=True)
            await drone.connect()
            await drone.start_video(connect=False)
            # await drone.takeoff()
            # await drone.turn_clockwise(360)
            # await drone.land()
            for i in range(0, 120):
                print(i)
                await asyncio.sleep(i)
        finally:
            await drone.stop_video()
            await drone.disconnect()

    loop = asyncio.new_event_loop()
    asyncio.set_event_loop(loop)
    loop.run_until_complete(main())

    print('[fly thread] END')


# os.makedirs("video_save_frames", exist_ok=True)

asyncio.get_child_watcher()

fly_thread = threading.Thread(target=fly, daemon=True)
fly_thread.start()

##############################################################################
# Video capture and GUI in main thread

print(f'[main thread] OpenCV capturing video from {tello_asyncio.VIDEO_URL}')
print(
    '[main thread] Press Ctrl-C or any key with the OpenCV window focussed'
    'to exit (the OpenCV window may take some time to close)'
)

capture = None
try:
    capture = cv2.VideoCapture(tello_asyncio.VIDEO_URL)
    capture.open(tello_asyncio.VIDEO_URL)

    idx = 0
    while True:
        # grab and show video frame in OpenCV window
        (grabbed, frame) = capture.read()
        print(grabbed)
        if grabbed:
            cv2.imshow('tello-asyncio', frame)
            # file_path = os.path.join("video_save_frames", f"{idx:05}.png")
            # print(file_path)
            # cv2.imwrite(file_path, frame)
            idx += 1

        # process OpenCV events and exit if any key is pressed
        r = cv2.waitKey(1)
        if r != -1 and r != 0xFF:
            break
except KeyboardInterrupt:
    pass
finally:
    # tidy up
    if capture:
        capture.release()
    cv2.destroyAllWindows()

print('[main thread] END')
